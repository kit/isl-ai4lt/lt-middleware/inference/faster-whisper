
from interface import FlaskInterface
from flask import request
from faster_whisper import WhisperModel
import tempfile
import json
import struct

inputs = [{"name":"audio","type":bytes},
          {"name":"input_language","type":str,"default":None},
          {"name":"output_language","type":str,"default":None},
          {"name":"prefix","type":str,"default":""},
          {"name":"priority","type":int,"default":0},
          {"name":"without_timestamps","type":bool,"default":False},
          {"name":"word_timestamps","type":bool,"default":False},
         ]

def initialize():
    print("Initializing")

    #model_size = "large-v2"
    model_size = "large-v3-turbo"

    # Run on GPU with FP16
    model = WhisperModel(model_size, device="cuda", compute_type="float16", download_root="/cache")

    return {"model": model, "max_batch_size":1}

def get_segment_info(s, word_timestamps):
    res = {"start":s.start, "end":s.end, "text":s.text}
    if word_timestamps:
        res["word_timestamps"] = [{"start":word.start, "end":word.end, "word":word.word, "probability":word.probability} for word in s.words]
    return res

def apply_model(inititalize_output, requests):
    model = inititalize_output["model"]

    req = requests[0]

    with tempfile.NamedTemporaryFile(dir="/dev/shm") as tmp_file:
        tmp_file.write(req.get("audio"))
        tmp_file.flush()

        in_lang = req.get("input_language")
        out_lang = req.get("output_language")
        task = "transcribe"
        if in_lang!=out_lang and out_lang=="en":
            task = "translate"
        prefix = req.get("prefix")
        word_timestamps = req.get("word_timestamps")

        segments, info = model.transcribe(tmp_file.name,
                                          language=in_lang,
                                          beam_size=5,
                                          no_repeat_ngram_size=6,
                                          prefix=prefix,
                                          without_timestamps=req.get("without_timestamps"),
                                          suppress_blank=not prefix,
                                          task=task,
                                          word_timestamps=word_timestamps)

    req.publish({"lid":info.language,
                 "lid_probs":info.all_language_probs,
                 "segments":[get_segment_info(s, word_timestamps) for s in segments]})

app = FlaskInterface(inputs,initialize,apply_model)
    
import struct

def add_wav_header(byte_stream, sample_rate=16000, num_channels=1, bits_per_sample=16):
    # Calculate necessary sizes
    num_samples = len(byte_stream) // (bits_per_sample // 8)
    byte_rate = sample_rate * num_channels * (bits_per_sample // 8)
    block_align = num_channels * (bits_per_sample // 8)
    subchunk2_size = num_samples * num_channels * (bits_per_sample // 8)
    chunk_size = 4 + (8 + 16) + (8 + subchunk2_size)

    # Construct the WAV header
    wav_header = struct.pack(
        '<4sI4s4sIHHIIHH4sI',
        b'RIFF',                # ChunkID
        chunk_size,             # ChunkSize
        b'WAVE',                # Format
        b'fmt ',                # Subchunk1ID
        16,                     # Subchunk1Size (16 for PCM)
        1,                      # AudioFormat (1 for PCM)
        num_channels,           # NumChannels
        sample_rate,            # SampleRate
        byte_rate,              # ByteRate
        block_align,            # BlockAlign
        bits_per_sample,        # BitsPerSample
        b'data',                # Subchunk2ID
        subchunk2_size          # Subchunk2Size
    )

    # Combine header and byte stream
    return wav_header + byte_stream

@app.route("/asr/infer/<input_language>,<output_language>", methods=["POST"])
def inference_legacy(input_language, output_language):
    data = request.files.to_dict()
    if not data:
        return "Invalid data", 400
    data["audio"] = add_wav_header(data.pop("pcm_s16le").read())
    data["without_timestamps"] = True
    res_, code = app.inference_(data)
    if code != 200:
        print(res_, code)
        res = {"hypo":""}
        return res, code
    res_ = json.loads(res_)
    res = {}
    lid = res_["lid"]
    res["lid"] = lid
    if input_language!="None" and not any(lid==l for l in input_language.split("+")):
        res["hypo"] = ""
    else:
        res["hypo"] = "".join(s["text"] for s in res_["segments"]).strip()
    return json.dumps(res), code

@app.route("/asr/available_languages", methods=["GET","POST"])
def languages():
    langs = ["en","zh","de","es","ru","ko","fr","ja","pt","tr","pl","ca","nl","ar","sv","it","id","hi","fi","vi","he","uk","el","ms","cs","ro","da","hu","ta","no","th","ur","hr","bg","lt","la","mi","ml","cy","sk","te","fa","lv","bn","sr","az","sl","kn","et","mk","br","eu","is","hy","ne","mn","bs","kk","sq","sw","gl","mr","pa","si","km","sn","yo","so","af","oc","ka","be","tg","sd","gu","am","yi","lo","uz","fo","ht","ps","tk","nn","mt","sa","lb","my","bo","tl","mg","as","tt","haw","ln","ha","ba","jw","su","yue"]
    return langs

app.run(port=5008)

# example call for offline transcriptionc:

# import requests
# mp3 = open("oneminute.mp3","rb").read()
# requests.post("http://localhost:5008/asr/inference", files={"audio":mp3}).text

# example call for lecture translator usage:

# import requests
# wav = open("short.wav","rb").read()[78:] # wav (1 channel, 16 kHz, pcm_s16le) without header
# requests.post("http://localhost:5008/asr/infer/None,None", files={"pcm_s16le":wav, "prefix":"Start of transcript"}).text

