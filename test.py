
class P:
    next_index = 0

    def __init__(self, p):
        self.priority = p

        self.index = P.next_index
        P.next_index += 1

    def __lt__(self, other):
        print(self.priority, self.index, other.priority, other.index)
        return (-self.priority, self.index) < (-other.priority, other.index)

import queue

if __name__ == "__main__":
    q = queue.PriorityQueue()
    q.put(P(1))
    q.put(P(0))

    print(q.get().priority)

