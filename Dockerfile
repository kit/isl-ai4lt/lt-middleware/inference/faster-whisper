
FROM ghcr.io/opennmt/ctranslate2:latest-ubuntu20.04-cuda11.2

# Set the working directory in the container
WORKDIR /usr/src/app

RUN pip install tokenizers==0.13.3

# Install any needed packages specified in requirements.txt
RUN pip install faster-whisper
RUN pip install Flask
RUN pip install --force-reinstall ctranslate2==3.24.0

COPY . .

# Run app.py when the container launches
#CMD python3 server.py
ENTRYPOINT ["python3", "-u", "server.py"]

